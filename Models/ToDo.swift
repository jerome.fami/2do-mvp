//
//  ToDo.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class ToDo: Object, Mappable {
    
    @objc dynamic var shortDescription: String? = nil
    @objc dynamic var longDescription: String? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        shortDescription    <- map["shortDescription"]
        longDescription    <- map["longDescription"]
    }
    
}
