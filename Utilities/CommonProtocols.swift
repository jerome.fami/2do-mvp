//
//  CommonProtocols.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation

protocol ErrorProtocol {
    func didReceiveError(errorMessage: String)
}

protocol FieldValidationProtocols {
    func validateFields(values: String?...) -> Bool
}
