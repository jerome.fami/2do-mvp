//
//  PersistentManager.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation
import RxSwift

class PersistentManager {
    
    init() {
        
    }
    
    func getToDoList() -> Single<[ToDo]> {
        let list = Array(realm.objects(ToDo.self))
        return Single.just(list)
    }
    
    func addToDo(toDo: ToDo) -> Single<[ToDo]> {
        try! realm.write {
            realm.add(toDo)
        }
        return getToDoList()
    }
    
    func updateToDo(toDo: ToDo, title: String, note: String) -> Completable {
        return Completable.create { (completable) -> Disposable in
            do {
                try realm.write {
                    toDo.shortDescription = title
                    toDo.longDescription = note
                }
                completable(.completed)
            } catch let error {
                completable(.error(error))
            }
            
            return Disposables.create()
        }
    }
    
}
