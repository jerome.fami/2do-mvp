//
//  Extensions.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func instantiateViewController(withIdentifier identifier: String, storyboardName: String, storyboardBunle: Bundle?) -> UIViewController {
        let storyBoard = UIStoryboard(name: storyboardName, bundle: storyboardBunle)
        let controller = storyBoard.instantiateViewController(withIdentifier: identifier)
        
        return controller
    }
    
}
