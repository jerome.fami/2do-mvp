//
//  AddPresenter.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation
import RxSwift

protocol AddPresenterDelegate: ErrorProtocol {
    func addPresenterAddSuccess(toDoList: [ToDo])
}

class AddPresenter {
    
    var delegate: AddPresenterDelegate?
    
    private var persistent: PersistentManager!
    private var disposeBag: DisposeBag!
    
    init() {
        self.persistent = PersistentManager()
        self.disposeBag = DisposeBag()
    }
    
    func addToDo(title: String?, notes: String?) {
        validateFields(title: title, notes: notes) { [unowned self] (error) in
            if let error = error {
                self.delegate?.didReceiveError(errorMessage: error)
            } else {
                let model = ToDo(JSON: ["shortDescription" : title!, "longDescription" : notes!])
                persistent.addToDo(toDo: model!).subscribe(onSuccess: { [unowned self] (updatedList) in
                    self.delegate?.addPresenterAddSuccess(toDoList: updatedList)
                }) { (error) in
                    self.delegate?.didReceiveError(errorMessage: error.localizedDescription)
                }.disposed(by: self.disposeBag)
            }
        }
    }
    
    private func validateFields(title: String?, notes: String?, completion: (_ errorMessage: String?) -> Void) {
        guard let title = title else {
            completion("Invalid fields!")
            return
        }
        guard let notes = notes else {
            completion("Invalid fields!")
            return
        }
        if title == "" || notes == "" {
            completion("Invalid fields!")
        } else {
            completion(nil)
        }
    }
    
}
