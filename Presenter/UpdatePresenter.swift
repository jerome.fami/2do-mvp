//
//  UpdatePresenter.swift
//  2doMVP
//
//  Created by Jerome Fami on 26/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation
import RxSwift

protocol UpdatePresenterDelegate: ErrorProtocol {
    func updatePresenterSuccess()
}

class UpdatePresenter: FieldValidationProtocols {
    
    var delegate: UpdatePresenterDelegate?
    
    private var persistent: PersistentManager!
    private var disposeBag: DisposeBag!
    
    init() {
        persistent = PersistentManager()
        disposeBag = DisposeBag()
    }
    
    func updateToDo(model: ToDo, title: String?, note: String?) {
        if validateFields(values: title), let title = title, let note = note {
            persistent.updateToDo(toDo: model, title: title, note: note).subscribe(onCompleted: {
                self.delegate?.updatePresenterSuccess()
            }) { (error) in
                self.delegate?.didReceiveError(errorMessage: error.localizedDescription)
            }.disposed(by: disposeBag)
        } else {
            delegate?.didReceiveError(errorMessage: "Invalid fields!")
        }
    }
    
    func validateFields(values: String?...) -> Bool {
        for value in values {
            guard let value = value else {
                return false
            }
            if value == "" {
                return false
            }
        }
        return true
    }
    
}
