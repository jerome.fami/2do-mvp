//
//  ToDoListPresenter.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import Foundation
import RxSwift

protocol ToDoListPresenterDelegate: ErrorProtocol {
    func toDoListPresenterDidUpdateList(toDos: [ToDo])
}

class ToDoListPresenter {
    
    var delegate: ToDoListPresenterDelegate?
    
    private var persistentManager: PersistentManager!
    private var disposeBag: DisposeBag!
    
    init() {
        self.persistentManager = PersistentManager()
        self.disposeBag = DisposeBag()
    }
    
    func getToDoList() {
        self.persistentManager.getToDoList().subscribe(onSuccess: { [unowned self] (list) in
            self.delegate?.toDoListPresenterDidUpdateList(toDos: list)
        }, onError: { [unowned self] (error) in
            self.delegate?.didReceiveError(errorMessage: error.localizedDescription)
        }).disposed(by: self.disposeBag)
    }
    
}
