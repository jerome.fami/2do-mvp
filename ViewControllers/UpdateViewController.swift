//
//  UpdateViewController.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import UIKit

private enum Constants {
    static let leftBarButtonTitleCancel = "Cancel"
    static let rightBarButtonTitleEdit = "Edit"
    static let rightBarButtonTitleUpdate = "SAVE"
}

protocol UpdateViewControllerDelegate {
    func updateViewControllerSuccessUpdate()
}

class UpdateViewController: UIViewController {

    @IBOutlet weak var navigationBarUpdate: UINavigationBar!
    @IBOutlet weak var buttonEditUpdate: UIBarButtonItem!
    @IBOutlet var buttonCancel: UIBarButtonItem!
    
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textFieldNotes: UITextView!
    
    var delegate: UpdateViewControllerDelegate?
    
    var presenter: UpdatePresenter?
    
    var model: ToDo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupFields()
        
        presenter = UpdatePresenter()
        presenter?.delegate = self
    }
    
    @IBAction func didPressEditUpdate(_ sender: UIBarButtonItem) {
        if let model = model {
            presenter?.updateToDo(model: model, title: textFieldTitle.text, note: textFieldNotes.text)
        }
        
    }
    @IBAction func didPressCancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupFields() {
        if let model = model {
            textFieldTitle.text = model.shortDescription
            textFieldNotes.text = model.longDescription
        }
    }
    
}

extension UpdateViewController: UpdatePresenterDelegate {
    
    func updatePresenterSuccess() {
        self.dismiss(animated: true) {
            self.delegate?.updateViewControllerSuccessUpdate()
        }
    }
    
    func didReceiveError(errorMessage: String) {
        let alertController = UIAlertController(title: "Oops!", message: errorMessage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
