//
//  AddViewController.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import UIKit

protocol AddViewControllerDelegate {
    func addViewControllerDidSave(toDoList: [ToDo])
}

class AddViewController: UIViewController {

    var delegate: AddViewControllerDelegate?
    
    private var presenter: AddPresenter?
    
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textViewNotes: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = AddPresenter()
        presenter?.delegate = self
    }

    @IBAction func didPressCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPressSave(_ sender: Any) {
        presenter?.addToDo(title: textFieldTitle.text, notes: textViewNotes.text)
    }
    
}

extension AddViewController: AddPresenterDelegate {
    
    func addPresenterAddSuccess(toDoList: [ToDo]) {
        self.dismiss(animated: true) {
            self.delegate?.addViewControllerDidSave(toDoList: toDoList)
        }
    }
    
    func didReceiveError(errorMessage: String) {
        let alertController = UIAlertController(title: "Oops!", message: errorMessage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
