//
//  ToDoListViewController.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import UIKit

private enum Constants {
    static let cellIdentifier = "Cell"
    static let cellNibName = "ToDoTableViewCell"
    
    static let addStoryboardName = "AddToDo"
    static let addControllerIdentifier = "AddViewController"
    
    static let updateStoryboardName = "UpdateToDo"
    static let updateControllerIdentifier = "UpdateViewController"
}

class ToDoListViewController: UIViewController {

    private var presenter: ToDoListPresenter?
    var toDoList = [ToDo]()
    
    @IBOutlet weak var tableViewList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ToDoListPresenter()
        presenter?.delegate = self
        presenter?.getToDoList()
        
        tableViewList.register(UINib(nibName: Constants.cellNibName, bundle: nil), forCellReuseIdentifier: Constants.cellIdentifier)
        tableViewList.dataSource = self
        tableViewList.delegate = self
    }
    
    @IBAction func didPressAdd(_ sender: Any) {
        let addController = instantiateViewController(withIdentifier: Constants.addControllerIdentifier, storyboardName: Constants.addStoryboardName, storyboardBunle: nil) as! AddViewController
        addController.delegate = self
        
        self.present(addController, animated: true, completion: nil)
    }
    
}

extension ToDoListViewController: ToDoListPresenterDelegate {
    
    func toDoListPresenterDidUpdateList(toDos: [ToDo]) {
        toDoList = toDos
        tableViewList.reloadData()
    }
    
    func didReceiveError(errorMessage: String) {
        let alertController = UIAlertController(title: "Oops!", message: errorMessage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension ToDoListViewController: AddViewControllerDelegate {
    
    func addViewControllerDidSave(toDoList: [ToDo]) {
        self.toDoList = toDoList
        tableViewList.reloadData()
    }
    
}

extension ToDoListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier, for: indexPath) as! ToDoTableViewCell
        let model = toDoList[indexPath.row]
        
        cell.updateCellValues(model: model)
        
        return cell
    }
    
}

extension ToDoListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let updateController = instantiateViewController(withIdentifier: Constants.updateControllerIdentifier, storyboardName: Constants.updateStoryboardName, storyboardBunle: nil) as! UpdateViewController
        let model = toDoList[indexPath.row]
        updateController.delegate = self
        updateController.model = model
        
        self.present(updateController, animated: true) {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
}

extension ToDoListViewController: UpdateViewControllerDelegate {
    func updateViewControllerSuccessUpdate() {
        tableViewList.reloadData()
    }
}
