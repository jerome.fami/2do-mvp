//
//  ToDoTableViewCell.swift
//  2doMVP
//
//  Created by Jerome Fami on 23/11/2018.
//  Copyright © 2018 Jerome Fami. All rights reserved.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {

    @IBOutlet weak var labelMainText: UILabel!
    @IBOutlet weak var labelSecondaryText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellValues(model: ToDo) {
        labelMainText.text = model.shortDescription
        labelSecondaryText.text = model.longDescription
    }
    
}
